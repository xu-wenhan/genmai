
package main

import (

"fmt"

"plugin"

"time"

)

/*

@author: mxd

@create time: 2020/10/5

*/

// main 主体程序入口

func main() {

// time.Now().Second 将会返回当前秒数

nowSecond := time.Now().Second()

doPrint(nowSecond)

fmt.Println("Process Stop ========")

}

// 执行打印操作

func doPrint(nowSecond int) {

if nowSecond%2 == 0 {

printWorld() //偶数

} else {

printHello() //奇数

}

}

// 执行打印hello

func printHello() {

// 执行插件调用

if pluginFunc != nil{

//将存储的信息转换为函数

if targetFunc, ok := pluginFunc.(func()); ok {

targetFunc()

}

}

fmt.Println("hello")

}

// 执行打印world

func printWorld() {

fmt.Println("world")

}

// 定义插件信息

const pluginFile = "HelloPlugin.so"

// 存储插件中将要被调用的方法或变量

var pluginFunc plugin.Symbol

// init 函数将于 main 函数之前运行

func init() {

// 查找插件文件

pluginFile, err := plugin.Open(pluginFile)

if err != nil {

fmt.Println("An error occurred while opening the plug-in")

} else{

// 查找目标函数

targetFunc, err := pluginFile.Lookup("PrintNowTime")

if err != nil {

fmt.Println("An error occurred while search target func")

}

pluginFunc = targetFunc

}

fmt.Println("Process On ==========")

}

#//////////////////////////////////////////////////////////////////
#//
#//       Filename:  Makefile
#//
#//        Version:  1.0
#//        Created:  2022年10月25日 23时10分18秒
#//       Revision:  none
#//
#//         Author:  alpha
#//   Organization:  alpha
#//       Contacts:  chenxinquan@kylinos.cn
#//
#//////////////////////////////////////////////////////////////////
#
#//////////////////////////////////////////////////////////////////
#// Description:
#//////////////////////////////////////////////////////////////////
#
#//////////////////////////////////////////////////////////////////
#// Log:
#//////////////////////////////////////////////////////////////////
#
#//////////////////////////////////////////////////////////////////
#// Todo:
#//
#//////////////////////////////////////////////////////////////////

PRO_NAME    = genmai
PRO_VERSION = 2022.10.25
#PRO_TOP_DIR=${.CURDIR}
PRO_TOP_DIR = $(shell pwd)

BUILD_DIR      = ${PRO_TOP_DIR}/build

SRC_DIR    = ${PRO_TOP_DIR}/src
BIN_DIR    = ${PRO_TOP_DIR}/bin

# TODO: make it can configurable
MAKE = make
GO_COMPILER := GO111MODULE=on go

all: build
build::
clean::
realclean::

################################################################
# pre
build::      pre-build

pre-build:
	if [ ! -d ${BUILD_DIR} ]; then mkdir ${BUILD_DIR}; fi

################################################################
# begin
build::      begin-build
clean::      begin-clean
realclean::  begin-realclean

begin-build:
	@echo
	@echo "----------------------------------------------------------------"
	@echo " >>> Building genmai (version ${PRO_VERSION})"
	@echo "----------------------------------------------------------------"

begin-clean:
	@echo
	@echo "----------------------------------------------------------------"
	@echo " >>> Cleaning project"
	@echo "----------------------------------------------------------------"

begin-realclean:
	@echo
	@echo "----------------------------------------------------------------"
	@echo " >>> Cleaning project"
	@echo "----------------------------------------------------------------"

################################################################
# genmai
build::      genmai-build
clean::      genmai-clean
realclean::  genmai-realclean

GENMAI_DIR=     ${PRO_TOP_DIR}/genmai

genmai-build: pre-build
	@echo ">"
	cd ${SRC_DIR} && ${GO_COMPILER} build -o ${BUILD_DIR}

genmai-clean:
	@echo ">"
	if [ -e ${BUILD_DIR} ]; then rm -rf ${BUILD_DIR}; fi

genmai-realclean:
	@echo ">"
	if [ -e ${BUILD_DIR} ]; then rm -rf ${BUILD_DIR}; fi

################################################################
# end
build::      end-build
clean::      end-clean
realclean::  end-realclean

end-build:
	@echo
	@echo "----------------------------------------------------------------"
	@echo " >>> Built genmai successfully"
	@echo "----------------------------------------------------------------"

end-clean:
	@echo "----------------------------------------------------------------"
	@echo " >>> Cleaned project successfully"
	@echo "----------------------------------------------------------------"

end-realclean:
	@echo "----------------------------------------------------------------"
	@echo " >>> Cleaned project successfully"
	@echo "----------------------------------------------------------------"

################################################################
# help
help:
	@echo
	@echo "----------------------------------------------------------------"
	@echo " >>> Help"
	@echo "----------------------------------------------------------------"
	@echo "Building targets:"
	@echo "all                  -- build genmai source tree."
	@echo "build                -- build all source tree."
	@echo
	@echo "Cleaning targets:"
	@echo "clean                -- remove 'build' directory"
	@echo "realclean            -- clean everything was built"
	@echo


################################################################
################################################################
# utils
run: build
	${BUILD_DIR}/main -system -kernel

test: clean build
	${BUILD_DIR}/main -system -kernel

.PHONY: all genmai


package main

import (
   "main/genmai/ArgParser"
   "main/genmai/FrameWorkCheck"
    "fmt"
   "flag"
   "log"
   "strconv"
   "strings"
    genmai "main/genmai"
    // gcon   "main/gconsole"
)
type Vul struct{
    ParserNum int  //协程数 
    System string  //执行系统漏洞检测
    Web string     //Web漏洞检测
    Kernel string  //内核漏洞检测
    Fuzz string    //Fuzz
    BaseLine string //基线检测
    Update string  //更新软件
    Docs  string  //生成报告
    PoolStatNum int //启动协程任务数
    IP string //web 制定IP
    MD string //生成MD文件
    RemoteAssessment string //远程检测,所需参数在RAVUL中
    WKPWD string //弱口令生成,所需参数在WKPWDVUL结构体中
    SSHBurst string //SSH爆破
    Nmap string //Nmap模块,端口和IP放在RAVUL中
    Fofa string //fofa接口调用，需要输入查询命令
    FastScan string //快速扫描模式/版本匹配
}

type FofaCommand struct{
    FofaCom string
}

type RAVUL struct{
    Host string //主机IP
    User string //用户名
    Password string //密码
    Port string //端口
}

type WKPWDVUL struct{
    CompanyName string //公司名
    Name string //名字
    Nums string //特殊数字
}

func main(){


    // /////////////////////////////
    // test "main/genmai"
    // genmai.Test()
    // config := genmai.NewConfig()
    // fmt.Println(">>")
    // fmt.Println(config["id"])
    // /////////////////////////////
    // test gconsole
    // gcon.Draw()

    // return


    var vul Vul //定义vul
    var RAV RAVUL
    var WKV WKPWDVUL
    var FC FofaCommand
    vul.PoolStatNum =0

    //开始日志打印日志
    genmai.LogInit()

    //检测系统架构
    frameWork:=FrameWorkCheck.FwCheck()
    if len(frameWork) > 0{
        log.Println("frameWork sure")
    }else{
        return
    }

    Help := flag.Bool("help", false,"")

    //识别参数，执行模块
    flag.IntVar(&vul.ParserNum, "poolNums", 100, "设置协程的数量，默认数量为0，最大数量为1000")
    flag.StringVar(&vul.Web, "web", "false", "使用web漏洞的验证模块，可联合其他模块使用")
    flag.StringVar(&vul.IP, "ip", "false", "设置ip，可设置ip段进行验证")
    flag.StringVar(&vul.System, "system", "false", "使用系统漏洞的验证模块，可联合其他模块使用")
    flag.StringVar(&vul.Kernel, "kernel", "false", "使用内核漏洞的验证模块，可联合其他模块使用")
    flag.StringVar(&vul.BaseLine, "baseline", "false", "使用基线检测模块，可联合其他模块使用")

    //远程模块参数
    RA := flag.Bool("RA", false, "使用远程检测，只能单独使用模块")
    flag.StringVar(&RAV.Host, "host", "false", "IP")
    flag.StringVar(&RAV.Port, "port", "all", "端口")
    flag.StringVar(&RAV.User, "user", "false", "用户名")
    flag.StringVar(&RAV.Password, "passwd", "false", "远程登录密码")
    


    //弱密码生成模块
    WK := flag.Bool("WKPWD", false, "使用弱口令生成器模块,选用参数CPN,Name,Nums")
    flag.StringVar(&WKV.CompanyName, "CPN", "0", "设置特定公司名")
    flag.StringVar(&WKV.Name, "Name", "0", "设置姓名")
    flag.StringVar(&WKV.Nums, "Nums", "0", "设置特殊数字（如年份）")

    // SSH爆破模块
    SSHB:= flag.Bool("SSHBurst", false, "使用SSH爆破,必用参数host,选用参数poolNums")

    // Nmap模块
    NmapScan:= flag.Bool("Nmap",false,"使用Nmap模块进行扫描,必用参数host,选用参数port")

    // Fofa模块
    Fofa:=flag.Bool("Fofa",false,"Fofa探测,必用参数FofaCom")
    flag.StringVar(&FC.FofaCom, "fofaCom", "null", "设置特定公司名")

    //版本匹配，快速扫描
    FastScan := flag.Bool("FastScan", false, "使用远程检测，只能单独使用模块")
    
    //
    All := flag.Bool("all", false, "只扫描system,kernel的所有poc以及检测baselin模块，不可联合其他参数使用")

    Update := flag.Bool("update", false, "更新程序到最新版本，不可联合其他参数使用")

    //flag解析
    flag.Parse()
    
    //将插件模块的值存放到数组中
    PWDList :=[...]string{WKV.CompanyName,WKV.Name,WKV.Nums}
    poolNums:=strconv.Itoa(vul.ParserNum)
    SSHBurstList :=[...]string{RAV.Host,poolNums} 
    NmapScanList :=[...]string{RAV.Host,RAV.Port}

    //初始化bool值
    sAll :=strconv.FormatBool(*All)
    vul.Update=strconv.FormatBool(*Update)
    vul.RemoteAssessment=strconv.FormatBool(*RA)
    vul.WKPWD=strconv.FormatBool(*WK)
    help:=strconv.FormatBool(*Help)
    vul.SSHBurst =strconv.FormatBool(*SSHB)
    vul.Nmap = strconv.FormatBool(*NmapScan)
    vul.Fofa = strconv.FormatBool(*Fofa)
    vul.FastScan = strconv.FormatBool(*FastScan)


    //是否开启远程检测
    if vul.RemoteAssessment=="true"{
        checkResult:=ArgParser.RemoteArgParser(RAV.Host,RAV.User,RAV.Password)
        checkResult=strings.TrimSpace(checkResult)
        if checkResult=="true"{
            fmt.Println("不允许登录root/administrator用户进行验证")
            log.Println("不允许登录root/administrator用户进行验证")
        }else if checkResult=="false"{
            ArgParser.ParameterParser(vul.System,vul.Kernel,vul.Web,vul.BaseLine,sAll,vul.PoolStatNum,vul.ParserNum,vul.Update,vul.IP,help)
        }

    }else{
        ArgParser.WKPWD(vul.WKPWD,PWDList[:])
        ArgParser.SSHBurst(vul.SSHBurst,SSHBurstList[:])
        ArgParser.NmapScan(vul.Nmap, NmapScanList[:])
        ArgParser.FofaApi(vul.Fofa,FC.FofaCom)
        ArgParser.SystemFastScan(vul.FastScan)
        ArgParser.ParameterParser(vul.System,vul.Kernel,vul.Web,vul.BaseLine,sAll,vul.PoolStatNum,vul.ParserNum,vul.Update,vul.IP,help)
    }
    return
}

module main

go 1.17

require (
	github.com/go-sql-driver/mysql v1.7.0
	github.com/google/uuid v1.3.0
	github.com/jesseduffield/gocui v0.3.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	golang.org/x/crypto v0.2.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/nsf/termbox-go v1.1.1 // indirect
)

require (
	github.com/mitchellh/mapstructure v1.5.0
	golang.org/x/sys v0.2.0 // indirect
)

////////////////////////////////////////////////////////////////
//
//       Filename:  ReportKernel.go
//
//        Version:  1.0
//        Created:  2022年11月17日 15时29分30秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package genmai

import (
    "time"
)

type
ReportKernel struct {
    ReportCommon
}
type
VulnInfoKernel struct {
    VulnInfoCommon
}

func
GetTemplateReportKernel() (*ReportKernel) {
    var expvuls []VulnInfoCommon
    expvuls = append(expvuls, GetTemplateVulnInfoKernel().VulnInfoCommon)
    /* */
    var expip4addr []string
    expip4addr = append(expip4addr, "0.0.0.0")
    var expip6addr []string
    expip6addr = append(expip6addr, "0.0.0.0")
    /* */
    return &ReportKernel {
                ReportCommon {
                    RCServerUUID:       "1234-5678-1234-5678",
                    RCServerName:       "aServer-Kernel",
                    RCFamily:           "RCFamily",
                    RCRelease:          "RCRelease",
                    RCContainer:        "RCContainer",
                    /* */
                    RCExploredTimeAt:   time.Now(),
                    RCExploredMode:     "RCExploredMode",
                    RCExploredVersion:  "RCExploredVersion",
                    RCExploredRevision: "RCExploredRevision",
                    RCExploredBy:       "RCExploredBy",
                    RCExploredVia:      "RCExploredVia",
                    RCExploredIPv4Addrs:expip4addr,
                    RCExploredIPv6Addrs:expip6addr,
                    /* */
                    RCReportedAt:       time.Now(),
                    RCReportedVersion:  "RCReportedVersion",
                    RCReportedBy:       "RCReportedBy",
                    /* */
                    RCErrors:           "RCErrors",
                    RCWarnings:         "RCWarnings",

                    RCExploredVulns:    expvuls,
                    RCReunningKernelInfo:ReportKernelInfo {
                                            "0.0",
                                            "0.0",
                                            false,
                                        },
                    RCPackages:         "RCPackages",      // TBD: type
                    RCSrcPackages:      "RCSrcPackages",      // TBD: type
                    RCOptional:         "RCOptional",      // TBD: type
                },
            }
}

func
GetTemplateVulnInfoKernel() VulnInfoKernel {
    return VulnInfoKernel {
                VulnInfoCommon {
                    VICFormatVer:       1,
                    VICId:              "VICId",
                    VICBelong:          "VICBelong",
                    VICPocHazardLevel:  "VICPocHazardLevel",
                    VICSource:          "VICSource",
                },
            }
}

////////////////////////////////////////////////////////////////
// ReportBase methods
func
(rk *ReportKernel)GetReportCommon() ReportCommon {
    return rk.ReportCommon
}

////////////////////////////////////////////////////////////////
//
//       Filename:  ConfigParser.go
//
//        Version:  1.0
//        Created:  2022年10月25日 17时53分41秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.cn
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package genmai

import (
    "errors"
)

var (
    RC_ERR_FILE_SYNTAX                                  error =
            errors.New("Syntax error")
    RC_ERR_FILE_TYPE                                    error =
            errors.New("This is a unknown type of the config file")
    RC_ERR_EMPTY_INTERFACE                              error =
            errors.New("The interface config is nil")
    RC_ERR_EMPTY_FILE                                   error =
            errors.New("The config file is nil")
)

type
ConfigParserBase interface {
    Parse(file string) (rc error)
    Mashal(file string) (rc error)
}

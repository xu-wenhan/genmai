package Pool

import (
//   "fmt"
//   "main/genmai/Cache"
  "sync"
  "log"
  "main/genmai"
)

func worker(task func(), wg *sync.WaitGroup) {
	defer wg.Done()
	task()
}
func kernel(){
	genmai.Kernel()
}
func system(){
	genmai.System()
}
func web(){
	genmai.Web()
}
func baseline(){
	genmai.BaseLine()
}

func CoprogramPool(Request map[string]string,ipList []string) {
	var wg sync.WaitGroup
	wg.Add(len(Request))

//  缓 存 获 取
//   cache:=Cache.SiteCache()
  if len(Request["kernel"]) > 0 {
    //获 取 kernel 缓 存 值
    // if kernel, found := cache.Get("kernel"); found {
	// 	// My:=kernel
	// 	kernelVul:=*(kernel.(*Cache.MyStruct))
	// 	KernelTaskNums=len(kernelVul.Msg)
    //   }
	go worker(kernel,&wg)
  }else{
    log.Println("未加载kernel模块")
  }

  if len(Request["system"]) > 0 {
    // if system, found := cache.Get("system"); found {
	// 	systemVul:=*(system.(*Cache.MyStruct))
	// 	// SystemTaskNums=len(systemVul.Msg)
    //   }

	go worker(system,&wg)

  }else{
    log.Println("未加载system模块")
  }


  if len(Request["web"]) > 0 && len(ipList)>0{
    // if web, found := cache.Get("web"); found {
	// 	webVul:=*(web.(*Cache.MyStruct))
	// 	// WebTaskNums=len(webVul.Msg)
    //   }
	go worker(web,&wg)

  }else{
    log.Println("未加载web模块")
  }


  if len(Request["baseline"]) > 0 {
    // if baseline, found := cache.Get("baseline"); found {
	// 	baselineVul:=*(baseline.(*Cache.MyStruct))
	// 	// BaseLineTaskNums=len(baselineVul.Msg)
    //   }
	go worker(baseline,&wg)

  }else{
    log.Println("未加载baseline模块")
  }
  wg.Wait() 
}
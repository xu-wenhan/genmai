/*
由于go语言的包引用问题（如果把Utils.go写成底层包，即跟用到它的包不同名），
将引发一长串包引用声明问题，比如要写成"/main/src/Sandbox",为以后包的位置
挪动带来不变，故从genmai复制了一份到Sandbox包。`^`
*/

////////////////////////////////////////////////////////////////
//
//       Filename:  Utils.go
//
//        Version:  1.0
//        Created:  2022年11月08日 21时05分02秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package Sandbox

import (
    "fmt"
    "os"
    "strings"
    // "log"
)

var A_DEBUG_LEVEL int = 0
var A_DEBUG_NEED_HEAD bool = true
////////////////////////////////////////////////////////////////
// ALOG
const (
    A_DEBUG_LEVEL_INFO          int     = 0
    A_DEBUG_LEVEL_NOTE          int     = 1
    A_DEBUG_LEVEL_WARNING       int     = 2
    A_DEBUG_LEVEL_ERROR         int     = 3

    A_DEBUG_LEVEL_STR_INFO      string  = "info"
    A_DEBUG_LEVEL_STR_NOTE      string  = "note"
    A_DEBUG_LEVEL_STR_WARNING   string  = "warning"
    A_DEBUG_LEVEL_STR_ERROR     string  = "error"
)
func
A_DEBUG(head string, args ...interface{}) {
    if (true == A_DEBUG_NEED_HEAD) {
        fmt.Print(head)
    }

    fmt.Println(args...)
}

func
A_DEBUG_INFO(args ...interface{}) {
    if (A_DEBUG_LEVEL <= A_DEBUG_LEVEL_INFO) {
        A_DEBUG(A_DEBUG_LEVEL_STR_INFO + ":", args...)
    }
}
func
A_DEBUG_NOTE(args ...interface{}) {
    if (A_DEBUG_LEVEL <= A_DEBUG_LEVEL_NOTE) {
        A_DEBUG(A_DEBUG_LEVEL_STR_NOTE + ":", args...)
    }
}
func
A_DEBUG_WARNING(args ...interface{}) {
    if (A_DEBUG_LEVEL <= A_DEBUG_LEVEL_WARNING) {
        A_DEBUG(A_DEBUG_LEVEL_STR_WARNING + ":", args...)
    }
}
func
A_DEBUG_ERROR(args ...interface{}) {
    if (A_DEBUG_LEVEL <= A_DEBUG_LEVEL_ERROR) {
        A_DEBUG(A_DEBUG_LEVEL_STR_ERROR + ":", args...)
    }
}

////////////////////////////////////////////////////////////////
// File , Path
func
GetPrefixPath(path string) string {
    index := strings.LastIndex(path, string(os.PathSeparator))
    /* */
    return path[:index]
}

func
GetSubfixPath(path string) string {
    index := strings.LastIndex(path, string(os.PathSeparator))
    /* */
    return path[(index+1):]
}

func
GetSubfixFile(path string) string {
    index := strings.LastIndex(path, ".")
    /* */
    return path[index:]
}

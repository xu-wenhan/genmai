
////////////////////////////////////////////////////////////////
//
//       Filename:  ExplorerCommon.go
//
//        Version:  1.0
//        Created:  2022年11月02日 11时24分43秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//                  songbangchengjin
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//                  songbangchengjin@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//  a. 计划写成工厂方法
////////////////////////////////////////////////////////////////

package genmai

import (
    "errors"
    "github.com/mitchellh/mapstructure"
)

var (
    RC_POC_CHECK_SUCCESSFULLY                   error =
            errors.New("Poc Check Successfully!")
    RC_POC_CHECK_FAILED                         error =
            errors.New("Poc Check Failed!")
)

var (
    ERR_STRUCT_NOTSETUP                                 error =
            errors.New("Struct has not setup yet!")
)

// Matchers   解析验证
type
Matchers struct{
    Type            string
    Condition       string
    MatcherMap      map[string]string
}

type
ImArray struct {
    Inter           string
    InterArgs       []string
    Exec            string
    Args            []string
}

//Implement   解析执行类型
type
Implement struct {
    ImArray         []ImArray
    ExpireTime      int         // POC运行时间限制（单位秒）
    Inter           []string    // POC标准输出输入交互
    Condition       string
}

// SiteRequests 解析请求中的值
type
SiteRequests struct {
    Implement
}

// SiteClassification 解析Info中的信息
type
SiteClassification struct {
    CvssMetrics         string
    CvssScore           float32
    CveId               string
    CweId               string
    CnvdId              string
    KveId               string
}

// Info  yaml文件的Info
type
SiteInfo struct {
    Name                string
    Severity            string
    Description         string
    ScopeOfInfluence    string
    References          []string
    SiteClassification  SiteClassification
    Tags                []string
}

/*
    Explorer的模板
*/
type
ConfigCommon struct {
    FormatVer           int
    Id                  string
    Belong              string
    PocHazardLevel      string
    Source              string
    SiteInfo            SiteInfo
    SiteRequests        SiteRequests
    /* */
    // other fields
}

///////////////////////////////
//
type
ExplorerConfigBase interface {
    // 每个结构体实现，应该完成这个函数以返回具体类型的Config
    // 由于需要用到具体类型，且因为go语言的限制，需要开发者编
    // 写具体类型。
    // 比如：
    // func (ek *ExplorerKernel)GetExplorerConfigKernel() (config *ExplorerConfigKernel, rc error) {
    //
    // GetExplorerConfig*() (config *Config*, rc error) {
}

type
ExplorerCommon struct {
    EcConfigParser      ConfigParserBase
    EcConfig            ExplorerConfigBase
    EcConfigFilePrefix  string
    /* */
    isSetup             bool
}

func
(ec *ExplorerCommon)Setup(parser ConfigParserBase,
                          config ExplorerConfigBase) {
    ec.EcConfigParser   = parser
    ec.EcConfig         = config
    /* */
    ec.isSetup          = true
}

func
(ec *ExplorerCommon)LoadConfig(file string) (rc error) {
    if (true != ec.isSetup) {
        return ERR_STRUCT_NOTSETUP
    }

    ec.EcConfigParser.Mashal(file)
    rc = mapstructure.Decode(ec.EcConfigParser, ec.EcConfig)
    if (nil != rc) {
        A_DEBUG_ERROR("mapstructure.Decode error ! rc =", rc)
        /* */
        return rc
    } // if (nil != ...

    ec.EcConfigFilePrefix = GetPrefixPath(file)

    return rc
}

///////////////////////////////
// ExplorerBase
type
ExplorerBase interface {
    Explore() (rp ReportBase, rc error)
}


///////////////////////////////
// auxiliary function

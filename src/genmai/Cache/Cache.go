package Cache

import (
    "log"
    "github.com/patrickmn/go-cache"
)

type MyStruct struct {
    Msg map[string]string
}

type Cache struct{
	KernelCache map[string]string
	WebCache map[string]string
	BaseLineCache map[string]string
	SystemCache map[string]string
}

func SiteCache() *cache.Cache{
    // 创建一个不会过期的缓存
    c := cache.New(0,0)
    // 声明map
	var cacheVul Cache
	cacheVul.KernelCache = make(map[string]string)
	cacheVul.BaseLineCache = make(map[string]string)
	cacheVul.WebCache = make(map[string]string)
	cacheVul.SystemCache = make(map[string]string)
	cacheVul.KernelCache["1"] = "宋江"
    cacheVul.KernelCache["2"] = "李逵"
	cacheVul.WebCache["1"] = "李逵2"

	//kernel 的poc存储到缓存
    c.Set("kernel", &MyStruct{Msg: cacheVul.KernelCache}, cache.DefaultExpiration)
	//system 的poc存储到缓存
	c.Set("system", &MyStruct{Msg: cacheVul.SystemCache}, cache.DefaultExpiration)
	//web 的poc存储到缓存
	c.Set("web", &MyStruct{Msg: cacheVul.WebCache}, cache.DefaultExpiration)
	//baseline 的poc存储到缓存
	c.Set("baseline", &MyStruct{Msg: cacheVul.BaseLineCache}, cache.DefaultExpiration)

    log.Println("缓存中.......")
	return c
}

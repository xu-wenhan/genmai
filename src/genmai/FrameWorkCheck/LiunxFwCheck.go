package FrameWorkCheck
import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"strings"
)

func LinuxFwCheck()string {

	cmdStr:="uname -a|awk '{print $1,$13}'"
	cmd := exec.Command("/bin/bash", "-c", cmdStr)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout // 标准输出
	cmd.Stderr = &stderr // 标准错误
	err := cmd.Run()
	outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	if len(errStr)!=0{
		fmt.Printf(errStr)
	}
	outStr=strings.TrimSpace(outStr)
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", errStr)
	}
return outStr

}

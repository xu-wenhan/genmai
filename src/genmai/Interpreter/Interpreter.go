////////////////////////////////////////////////////////////////
//
//       Filename:  Interpreter.go
//
//        Version:  1.0
//        Created:  2022年11月07日 11时57分41秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package Interpreter

import (
)

const (
    // 注释的属于"不定义命令",即命令不合理
    INTERPRETER_CMD_LEN                         int     = 3
    INTERPRETER_CMD_SEND                        string  = "<<:"
    INTERPRETER_CMD_RECV                        string  = ">>:"
    //INTERPRETER_CMD_SENDUNTIL                   string  = "<.:"
    INTERPRETER_CMD_RECVUNTIL                   string  = ">.:"
    //INTERPRETER_CMD_SEND_POC_CHECK              string  = "<?:"
    INTERPRETER_CMD_RECV_POC_CHECK              string  = ">?:"
    INTERPRETER_CMD_RECV_POC_CHECK_RC           string  = "??:"
)
////////////////////////////////////////////////////////////////
// Interpreter interface
type
Interpreter interface {
    Process(interpreter string,interArgs []string,exec string, args...string) (InterIO, error)
}
//,interArgs string
////////////////////////////////////////////////////////////////
//
//       Filename:  ReportWeb.go
//
//        Version:  1.0
//        Created:  2022年11月17日 18时06分56秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package genmai

import (
    "time"
)

type
ReportWeb struct {
    ReportCommon
}

type
VulnInfoWeb struct {
    VulnInfoCommon
}


func
GetTemplateReportWeb() (*ReportWeb) {
    var expvuls []VulnInfoCommon
    expvuls = append(expvuls, GetTemplateVulnInfoWeb().VulnInfoCommon)
    expvuls = append(expvuls, GetTemplateVulnInfoWeb().VulnInfoCommon)
    expvuls = append(expvuls, GetTemplateVulnInfoWeb().VulnInfoCommon)
    /* */
    var expip4addr []string
    expip4addr = append(expip4addr, "0.0.0.0")
    var expip6addr []string
    expip6addr = append(expip6addr, "0.0.0.0")
    /* */
    return &ReportWeb{
                ReportCommon {
                    RCServerUUID:       "1234-5678-1234-5678",
                    RCServerName:       "aServer-Web",
                    RCFamily:           "RCFamily",
                    RCRelease:          "RCRelease",
                    RCContainer:        "RCContainer",
                    /* */
                    RCExploredTimeAt:   time.Now(),
                    RCExploredMode:     "RCExploredMode",
                    RCExploredVersion:  "RCExploredVersion",
                    RCExploredRevision: "RCExploredRevision",
                    RCExploredBy:       "RCExploredBy",
                    RCExploredVia:      "RCExploredVia",
                    RCExploredIPv4Addrs:expip4addr,
                    RCExploredIPv6Addrs:expip6addr,
                    /* */
                    RCReportedAt:       time.Now(),
                    RCReportedVersion:  "RCReportedVersion",
                    RCReportedBy:       "RCReportedBy",
                    /* */
                    RCErrors:           "RCErrors",
                    RCWarnings:         "RCWarnings",

                    RCExploredVulns:    expvuls,
                    RCReunningKernelInfo:ReportKernelInfo {
                                            "0.0",
                                            "0.0",
                                            false,
                                        },
                    RCPackages:         "RCPackages",      // TBD: type
                    RCSrcPackages:      "RCSrcPackages",      // TBD: type
                    RCOptional:         "RCOptional",      // TBD: type
                },
            }
}

func
GetTemplateVulnInfoWeb() (*VulnInfoWeb) {
    return &VulnInfoWeb {
                VulnInfoCommon {
                    VICFormatVer:       1,
                    VICId:              "VICId",
                    VICBelong:          "VICBelong",
                    VICPocHazardLevel:  "VICPocHazardLevel",
                    VICSource:          "VICSource",
                },
            }
}

////////////////////////////////////////////////////////////////
// ReportBase methods
func
(rw *ReportWeb)GetReportCommon() ReportCommon {
    return rw.ReportCommon
}

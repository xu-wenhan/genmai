package genmai

import(
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"strings"
)
func PythonParser(Map map[int]string){
	var cmdStr string
	for i:=0;i<len(Map);i++{
		if i==0{
			cmdStr="python3 tools/"+Map[0]+".py"
			log.Println("tools文件名传入")
		}else{
			cmdStr=cmdStr+Map[i]
		}
	}	
	cmd := exec.Command("/bin/bash", "-c", cmdStr)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout // 标准输出
	cmd.Stderr = &stderr // 标准错误
	err := cmd.Run()
	outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	if len(errStr)!=0{
		fmt.Printf(errStr)
	}
	outStr=strings.TrimSpace(outStr)
	if err != nil {
		log.Fatalf("PythonParser worth %s\n", errStr)
	}

}
////////////////////////////////////////////////////////////////
//
//       Filename:  genmai.go
//
//        Version:  1.0
//        Created:  2022年10月26日 01时00分32秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.cn
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package genmai

import (
"fmt"
)

func Kernel() {

    DoctorIns().Reset()
    DoctorIns().LoadExplorersListConfig("../data/KernelPocs/KernelPocs.yaml")

    A_DEBUG_INFO(">>Genmai Kernel>>")
    DoctorIns().GenmaiKernel()

}
func System() {

    DoctorIns().Reset()
    DoctorIns().LoadExplorersListConfig("../data/SystemPocs/SystemPocs.yaml")

    A_DEBUG_INFO(">>Genmai System>>")
    DoctorIns().GenmaiSystem()
}
func Web() {
    fmt.Println("web")
}
func BaseLine(){

    DoctorIns().Reset()
    DoctorIns().LoadExplorersListConfig("../data/BaseLine/BaseLine.yaml")

    A_DEBUG_INFO(">>Genmai BaseLine>>")
    DoctorIns().GenmaiBaseline()
}

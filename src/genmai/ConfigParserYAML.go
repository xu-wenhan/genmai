////////////////////////////////////////////////////////////////
//
//       Filename:  ConfigParserYAML.go
//
//        Version:  1.0
//        Created:  2022年10月25日 17时56分41秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.cn
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package genmai

import (
    "os"
    "io"
    "strings"
    yaml "gopkg.in/yaml.v2"
)

////////////////////////////////////////////////////////////////
// ConfigParserYAML

// 继承于 ConfigParserBase接口
type
ConfigParserYAML map[string]interface {}

///////////////////////////////
// override ConfigParserBase functions
func
(cpy *ConfigParserYAML)Parse(file string) (rc error) {
    configfile, ok := os.Open(file)
    rc = ok;
    /* */
    if nil != rc {
        return rc
    }
    /* */
    defer configfile.Close()

    decoder := yaml.NewDecoder(configfile)

    for (nil == rc) { // decode yaml file if nil != rc
        config := ConfigParserYAML{}
        rc = decoder.Decode(config)
        /* */
        if (nil != rc) && (io.EOF != rc) {
            return rc
        } // if (nil != ...
        if (0 != len(config)) {
            (*cpy) = config
        } // if (0 != ...
    } // for (nil == ...

    return rc
}

func
(cpy *ConfigParserYAML)Mashal(file string) (rc error) {
    if (nil == cpy) {
        rc = RC_ERR_EMPTY_INTERFACE
        return rc
    } // if (nil == cpy ...

    if ("" == file) {
        rc = RC_ERR_EMPTY_FILE
        return rc
    } // if ("" == file

    if ( (strings.HasSuffix(file, ".yaml")) ||
         (strings.HasSuffix(file, ".yml"))     ) {
        rc = cpy.Parse(file)
//        if ( (0 == len("")) &&
//             (nil == err)       fmt.Println(rc)    ) {
//            cpys := &ConfigParserYAML{}
//            cpy = cpys
//            return cpy.Parse(file)
//        } // if ( (0 ...

        return rc
    } // if ( (strings ...

    rc = RC_ERR_FILE_TYPE
    return rc
}

package ArgParser

import(
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"log"
)

func ArgCheck(targetString string)(LegalValue string){
	match, err := regexp.MatchString(`^[a-zA-Z][a-zA-Z0-9-]{4,15}$`, targetString)
	   if err != nil {
		  fmt.Println("参数不合规")
	   }
	//    fmt.Println(match)
	   LegalValue=strconv.FormatBool(match)
   return LegalValue
}

//IP校验
func IPCheck(IP string)(matchResult string,List []string){
	List =  make([]string,0)
	checkIP:=strconv.FormatBool(strings.Contains(IP, "/24"))
	if checkIP =="true"{
		//解析D段
		matchResult,List=IPParserD(IP)
	}else{
		match, err := regexp.MatchString(`^((0|[1-9]\d?|1\d\d|2[0-4]\d|25[0-5])\.){3}(0|[1-9]\d?|1\d\d|2[0-4]\d|25[0-5])$`, IP)
		   if err != nil {
			  fmt.Println(IP," 不合法请修改")
		   }
		   // fmt.Println(match)
		matchResult=strconv.FormatBool(match)
		List=append(List,IP)
	}
	return matchResult,List
}
//D段解析
func IPParserD(IP string)(matchResult string,List []string){
	List =  make([]string,0)
	IP=strings.TrimRight(IP, "0/24")
	for i:=0;i<=255;i++{
		vul:=strconv.Itoa(i)
		IPVul:=IP+vul
		match, err := regexp.MatchString(`^((0|[1-9]\d?|1\d\d|2[0-4]\d|25[0-5])\.){3}(0|[1-9]\d?|1\d\d|2[0-4]\d|25[0-5])$`, IPVul)
		   if err != nil {
			  fmt.Println(IPVul," 不合法请修改")
		   }
		matchResult=strconv.FormatBool(match)
		if matchResult=="true"{
			List=append(List,IPVul)
		}else{
			log.Println("D段解析错误")
		}
	}
	return matchResult,List
}
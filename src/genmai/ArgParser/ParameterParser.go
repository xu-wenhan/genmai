package ArgParser

import(
	"flag"
	"fmt"
	"main/genmai/Pool"
    "main/genmai/RemoteCheck"
    "strings"
    "strconv"
    "log"
    "main/genmai"
    "main/tools/SSHExplosion"
    "main/tools/FastScan"
	)

var Num int
func ParameterParser(sSystem string,sKernel string,sWeb string,sBaseLine string,sAll string,PoolStatNum int,ParserNum int,Update string ,IP string,help string){
    Request:=make(map[string]string)
    var ipCheck string
    ipList :=  make([]string,0)
    Num=0
    if sSystem!="false"{
        if sSystem=="All" || sSystem=="all"{
            Request["system"] = sSystem
            Num=Num+1
        }else{
            //检测函数检测安全性
            //单独检查某个项目
            LegalSystem:=ArgCheck(sSystem)
            if LegalSystem=="true"{
                Request["system"] = sSystem
                Num=Num+1
            }else{
                fmt.Println("system参数不合规")
            }
        }
    }

    if sKernel!="false"{
        if sKernel=="All" || sKernel=="all"{
            Request["kernel"] = sKernel
            Num=Num+1
        }else{
            //检测函数检测安全性
            //单独检查某个项目
            LegalKernel:=ArgCheck(sKernel)
            if LegalKernel=="true"{
                Request["kernel"] = sKernel
                Num=Num+1
            }else{
                fmt.Println("kernel参数不合规")
            }
        }
        
    }

    if sWeb!="false" && IP!="false"{
        if sWeb=="All" || sWeb=="all"{
            ipCheck,ipList=IPCheck(IP)
            if ipCheck=="true"{
                Request["web"] = sWeb
                Num=Num+1
            }else{
                fmt.Println("IP参数不合规")
            }
        }else{
            //检测函数检测安全性
            //单独检查某个项目
            LegalWeb:=ArgCheck(sWeb)
            ipCheck,ipList=IPCheck(IP)
            if ipCheck=="true"&&LegalWeb=="true"{
                Request["web"] = sWeb
                Num=Num+1
            }else{
                fmt.Println("参数不合规")
            }
        }
    }else if sWeb!="false" && IP=="false"{
        fmt.Println("请输入IP参数")
        return
    }else if sWeb=="false" && IP!="false"{
        fmt.Println("请输入web参数")
        return
    }


    if sBaseLine!="false"{
        if sBaseLine=="All" || sBaseLine=="all"{
            Request["baseline"] = sBaseLine
            Num=Num+1
        }else{
            //检测函数检测安全性
            //单独检查某个项目
            LegalBaseline:=ArgCheck(sBaseLine)
            if LegalBaseline=="true"{
                Request["baseline"] = sBaseLine
                Num=Num+1
            }else{
                fmt.Println("BaseLine参数不合规")
            }
        }
    }

    if sAll=="true"{
        if sSystem=="false"&&sKernel=="false"&&sWeb=="false"&&sBaseLine=="false" {
            Request["kernel"] = sKernel
            Request["system"] = sSystem
            Request["baseline"] = sBaseLine
        }else{
            fmt.Println("Only allow all")
            return
        }
    }

    if Update=="true"{
        if sSystem=="false"&&sKernel=="false"&&sWeb=="false"&&sBaseLine=="false" {
            fmt.Println("Updating")
            return
        }else{
            fmt.Println("The update process does not allow other processes")
            return
        }
    }

    if help=="true"{
        flag.PrintDefaults()
    }

    if sSystem=="false"&&sKernel=="false"&&sWeb=="false"&&sBaseLine=="false"&&sAll!="true"&&Update!="true"&&help!="true"{
        fmt.Println("err,Please enter parameters")
        return
    }

    //协程最大值为200
    if ParserNum<0 || ParserNum>500{
        fmt.Println("协程最大值为0-500")
        return
    }else{
        poolNum:=ParserNum
        fmt.Println("info:Genmai enable default coprocess",poolNum)
    }
    Pool.CoprogramPool(Request,ipList)
}

//远程参数解析
func RemoteArgParser(SSHHost string,SSHUser string,SSHPassword string)(checkResult string){
    if SSHHost !="false" && SSHUser !="false" &&SSHPassword !="false"{
        SSHHostCheck,list:=IPCheck(SSHHost)
            if SSHHostCheck !="true"{
                fmt.Println("host错误",list)
            }else{
                command:="whoami"
                result:=RemoteCheck.SSHConnect(SSHHost,SSHUser,SSHPassword,command)
                checkResult=strconv.FormatBool(strings.Contains(result, "root"))
                //windows高权校验
                if checkResult!="true"{
                    checkResult=strconv.FormatBool(strings.Contains(result, "administrator"))
                }
                if checkResult!="true"{
                    //SCP 文件
                    SCPFile:=RemoteCheck.SCPFile()
                    if SCPFile =="true"{
                        log.Println("scp file success")
                    }else{
                        log.Println("scp file fail")
                    }
                }
            }
    }
    return checkResult
}
//弱口令模块参数解析
func WKPWD(WKPWD string ,PWDList []string){
    PWD:=make(map[int]string)
    j:=0
    PWD[0] ="WeakPwdGeneration/WeakPwdGeneration" //文件名
    if WKPWD=="true"{
        for i:=0;i<len(PWDList);i++{
            if PWDList[i]!="0" && i==0{
                j=j+1
                vul0:=" --CompanyName "+PWDList[0]
                PWD[j]=vul0
            }
            if PWDList[i]!="0" && i==1{
                j=j+1
                vul1:=" --Name "+PWDList[1]
                PWD[j]=vul1
            }
            if PWDList[i]!="0" && i==2{
                j=j+1
                vul2:=" --Nums "+PWDList[2]
                PWD[j]=vul2
            }
        }
        genmai.PythonParser(PWD)
        fmt.Println("弱密码已生成")
    }
    return
}
//SSH爆破
func SSHBurst (SSHBurst string,SSHBurstList []string){
    if SSHBurst =="true" {
        if SSHBurstList[0]!="false"{
            SSHHostCheck,list:=IPCheck(SSHBurstList[0])
            if SSHHostCheck!="true"{
                fmt.Println("SSH模块 host格式报错",list)
                log.Println("SSH模块 host格式报错")
            }else{
                poolNums,err:= strconv.Atoi(SSHBurstList[1])
                if err!=nil{
                    fmt.Println(err)
                    log.Println(err)
                }else{
    
                    SSHExplosion.SshExp(list[:],poolNums)
                    
                }
            }
        }else{
            fmt.Println("SSH爆破模块,参数使用错误,请添加host参数")
            log.Println("SSH爆破模块,参数使用错误,请添加host参数")
        }
        }
    return
}
//Nmap模块解析
func NmapScan(Nmap string, NmapScanList []string){
    Map:=make(map[int]string)
    Map[0]="Nmap/nmapScan"
    if Nmap=="true"{
        if NmapScanList[0]!="false"{
            if NmapScanList[1]=="all"{
                Map[1]=" -H "+NmapScanList[0]+" -p "+"0-65000"
                genmai.PythonParser(Map)

        }else{

            Map[1]=" -H "+NmapScanList[0]+" -p "+NmapScanList[1]
            genmai.PythonParser(Map)
        }
        fmt.Println("Nmap模块结束...")
        }else{
            fmt.Println("Nmap模块,参数使用错误,请添加host参数")
            log.Println("Nmap模块,参数使用错误,请添加host参数")
        }
}
return
}
func FofaApi(Fofa string,fofaCom string){
    Map:=make(map[int]string)
    Map[0]="Fofa/FofaApi"
    if Fofa=="true"{
        if fofaCom!="null"{
            Map[1]=" -c "+fofaCom
            genmai.PythonParser(Map)
        }else{
            fmt.Println("Fofa模块,参数使用错误,请添加fofaCom参数")
            log.Println("Fofa模块,参数使用错误,请添加fofaCom参数")
        }
        fmt.Println("Fofa模块结束...")
    }
return
}

func SystemFastScan(scan string){
    if scan =="true"{
        FastScan.GetdpkgInfo()
        fmt.Println("版本匹配检测结束...")
    }
    return
}
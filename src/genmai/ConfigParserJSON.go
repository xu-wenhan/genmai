////////////////////////////////////////////////////////////////
//
//       Filename:  ConfigParserJSON.go
//
//        Version:  1.0
//        Created:  2022年10月25日 17时54分34秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.cn
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package genmai

import (
    "os"
    "io"
    "strings"
    json "encoding/json"
)

////////////////////////////////////////////////////////////////
// ConfigParserJSON

// 继承于 ConfigParserBase接口
type
ConfigParserJSON map[string]interface {}

///////////////////////////////
// override ConfigParserBase functions
func
(cpj *ConfigParserJSON)Parse(file string) (rc error) {
    configfile, ok := os.Open(file)
    rc = ok;
    /* */
    if nil != rc {
        return rc
    }
    /* */
    json_bytes, rc := io.ReadAll(configfile)
    defer configfile.Close()

    config := ConfigParserJSON{}

    rc = json.Unmarshal(json_bytes, &config)

    (*cpj) = config

    return rc
}

func
(cpj *ConfigParserJSON)Mashal(file string) (rc error) {
    if (nil == cpj) {
        rc = RC_ERR_EMPTY_INTERFACE
        return rc
    } // if (nil == cpj ...

    if ("" == file) {
        rc = RC_ERR_EMPTY_FILE
        return rc
    } // if ("" == file

    if ( (strings.HasSuffix(file, ".json")) ) {
        rc = cpj.Parse(file)
//        if ( (0 == len("")) &&
//             (nil == err)       ) {
//            cpjs := &ConfigParserJSON{}
//            cpj = cpjs
//            return cpj.Parse(file)
//        } // if ( (0 ...

        return rc
    } // if ( (strings ...

    rc = RC_ERR_FILE_TYPE
    return rc
}

///////////////////////////////
// member functions

////////////////////////////////////////////////////////////////
//
//       Filename:  ConfigParserDefault.go
//
//        Version:  1.0
//        Created:  2022年11月01日 20时28分13秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package genmai

import (
    //"errors"
    //"io/ioutil"
    "os"
    "io"
    "strings"
    yaml "gopkg.in/yaml.v2"
)

////////////////////////////////////////////////////////////////
// ConfigParserDefault

/*
    The default config
    You can implement config, if you do not implement, it will
    use this default config that will be used for Configuration
    Center in the future
*/
// 继承于 ConfigParserBase接口
type
ConfigParserDefault map[string]interface {}

///////////////////////////////
// override ConfigParserBase functions
func
(cpd *ConfigParserDefault)Parse(file string) (rc error) {
    configfile, ok := os.Open(file)
    rc = ok;
    /* */
    if nil != rc {
        return rc
    }
    /* */
    defer configfile.Close()

    decoder := yaml.NewDecoder(configfile)
    for (nil == rc) { // decode yaml file if nil != rc
        config := ConfigParserDefault{}
        rc = decoder.Decode(config)
        /* */
        if (nil != rc) && (io.EOF != rc) {
            return rc
        } // if (nil != ...
        if (0 != len(config)) {
            (*cpd) = config
        } // if (0 != ...
    } // for (nil == ...

    return rc
}

func
(cpd *ConfigParserDefault)Mashal(file string) (rc error) {
    if (nil == cpd) {
        rc = RC_ERR_EMPTY_INTERFACE
        return
    } // if (nil == cpd ...

    if ("" == file) {
        rc = RC_ERR_EMPTY_FILE
        return
    } // if ("" == file

    if ( (strings.HasSuffix(file, ".yaml")) ||
         (strings.HasSuffix(file, ".yml"))     ) {
        rc = cpd.Parse(file)
//        if ( (0 == len("")) &&
//             (nil == err)       ) {
//            cpds := &ConfigParserDefault{}
//            cpd = cpds
//            return cpd.Parse(file)
//        } // if ( (0 ...

        return rc
    } // if ( (strings ...

    rc = RC_ERR_FILE_TYPE
    return rc
}

///////////////////////////////
// member functions


////////////////////////////////////////////////////////////////
// others
func
NewConfig() ConfigParserBase {
    return &ConfigParserDefault{}
}

package RemoteCheck

import (

    "fmt"

    "golang.org/x/crypto/ssh"

    "log"

    "time"

)

func SSHConnect(SSHHost string,SSHUser string, SSHPassword string,command string)(result string){

    sshHost := SSHHost

    sshUser := SSHUser

    sshPassword := SSHPassword

    sshType := "password"

    sshPort := 22

    //创建sshp登陆配置

    config := &ssh.ClientConfig{

        Timeout:         5*time.Second,//ssh 连接time out 时间一秒钟, 如果ssh验证错误 会在一秒内返回

        User:            sshUser,

        HostKeyCallback: ssh.InsecureIgnoreHostKey(), //这个可以, 但是不够安全

        //HostKeyCallback: hostKeyCallBackFunc(h.Host),

    }

    if sshType == "password" {

        config.Auth = []ssh.AuthMethod{ssh.Password(sshPassword)}

    }

    //dial 获取ssh client

	addr := fmt.Sprintf("%s:%d", sshHost, sshPort)

    sshClient, err := ssh.Dial("tcp", addr, config)

    if err != nil {

        log.Fatal("创建ssh client 失败",err)


    }

    defer sshClient.Close()

    //创建ssh-session

    session, err := sshClient.NewSession()

    if err != nil {

        log.Fatal("创建ssh session 失败",err)
    

    }

    defer session.Close()

    //执行远程命令
    combo,err := session.CombinedOutput(command)

    if err != nil {

        log.Fatal("远程执行cmd 失败",err,command)
        

    }
    log.Println("ssh connect succ")
    defer session.Close()
    result=string(combo)
    return result
}
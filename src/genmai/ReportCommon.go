////////////////////////////////////////////////////////////////
//
//       Filename:  ReportCommon.go
//
//        Version:  1.0
//        Created:  2022年11月08日 16时32分58秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package genmai

import (
    "time"
)

type
ReportContianer struct {
    RCContainerID           string
    RCName                  string
    RCImage                 string
    RCType                  string
    RCUUID                  string
}

type
ReportPlatform struct {
    RPName                  string
    RPInstanceID            string
}

type
ReportKernelInfo struct {
    RKRelease               string
    RKVersion               string
    RKRebootRequired          bool
}

type
ReportCommon struct {
    RCServerUUID            string
    RCServerName            string
    RCFamily                string
    RCRelease               string
    RCContainer             string
    /* */
    RCExploredType          string
    RCExploredTimeAt        time.Time
    RCExploredMode          string
    RCExploredVersion       string
    RCExploredRevision      string
    RCExploredBy            string
    RCExploredVia           string
    RCExploredIPv4Addrs     []string
    RCExploredIPv6Addrs     []string
    /* */
    RCReportedAt            time.Time
    RCReportedVersion       string
    RCReportedBy            string
    /* */
    RCElapsedTime           time.Duration // 单位：秒
    /* */
    RCErrors                string
    RCWarnings              string

    RCExploredVulns         []VulnInfoCommon
    RCReunningKernelInfo    ReportKernelInfo
    RCPackages              string      // TBD: type
    RCSrcPackages           string      // TBD: type
    RCOptional              string      // TBD: type
}

type
ReportBase interface {
    GetReportCommon() ReportCommon
}

////////////////////////////////////////////////////////////////
// VulnInfo


// TODO:TBD: 为以后VulnInfo被VulnInfoKernel, VulnInfoSystem继承做
//           准备
//type VulnInfoBase interface {
//    GetVulnInfoCommon
//}

// 打算取消这个结构
//// TODO: to complete
//type
//VulnInfoCommon struct {
//    VICCveId                 string
//    VICConfidences           Confidence
//    VICAffectedPackages      []PackageFixStatus
//    VICDistroAdvisories      []DistroAdvisory
//    VICCveContents           []CveContent
//    VICExploitInfos          []ExploitInfo
//
//    // VICMetasploits
//    VICMitigationInfos       []MitigationInfo
//    VICCtis                  []string
//    VICAlertDict             AlerDict
//    VICCpeURIs               []string
//    VICVulnType              string
//    // TODO: TBD
//}

type
VulnInfoCommon struct {
    VICFormatVer           int
    VICId                  string
    VICBelong              string
    VICPocHazardLevel      string
    VICSource              string
    VICSiteInfo            SiteInfo
    VICSiteRequests        SiteRequests
    /* */
    // other fields
}

type
Confidence struct {
    CScore                  float32
    CDetectionMethod        DetectionMethod
    SortOrder               int
}

type
DetectionMethod string

type
PackageFixStatus struct {
    PFSName                 string
    PFSNotFixedYet          bool
    PFSFixState             string
    PFSFixedIn              string
}

type
DistroAdvisory struct {
    DAAdvisoryId            string
    DASeverity              string
    DAIssued                time.Time
    DAUpdated               time.Time
    DADescription           string
}

// TODO:
type
CveContent struct {
}

type
ExploitInfo struct {
    EIExploitType           string // TODO: TBD
    EIId                    string
    EIURL                   string
    EIDescription           string
    EIDocumentURL           []string
    EIShellCodeURL          []string
    EIBinaryURL             []string
}

type
MitigationInfo struct {
    // MICveContentType
    MIMitigationInfo        string
    MIURL                   string
}

type
AlertInfo struct {
    AIURL                   string
    AITitle                 string
    AITeam                  string
}

type
AlerDict struct {
    ADCISA                  []AlertInfo
    ADJPCERT                []AlertInfo
    ADUSCERT                []AlertInfo
}

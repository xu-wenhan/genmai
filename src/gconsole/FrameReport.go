////////////////////////////////////////////////////////////////
//
//       Filename:  FrameReport.go
//
//        Version:  1.0
//        Created:  2022年11月16日 23时12分05秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package gconsole

import (
    //"github.com/jroimartin/gocui"
    "main/genmai"
)

type
FrameReport struct {
    reports             []genmai.ReportBase
    curReport           genmai.ReportBase
    vulnInfos           []genmai.VulnInfoCommon
    curVuln         int
    curVulnViewLimitY   int
    curReportViewLimitY     int
}

var G_FrameReport FrameReport

// TODO: may be should remove this function
func
(fr *FrameReport)Setup(rps []genmai.ReportBase) {
//    fr.reports = append(fr.reports, genmai.GetTemplateReportKernel())
//    fr.reports = append(fr.reports, genmai.GetTemplateReportKernel())
//    /* */
//    fr.reports = append(fr.reports, genmai.GetTemplateReportDBus())
//    fr.reports = append(fr.reports, genmai.GetTemplateReportDBus())
//    /* */
//    fr.reports = append(fr.reports, genmai.GetTemplateReportSystem())
//    fr.reports = append(fr.reports, genmai.GetTemplateReportSystem())
    /* */
    fr.reports = rps
    fr.curReport = rps[0]
    fr.curVuln                  = 0
    fr.curVulnViewLimitY        = 1
    fr.curReportViewLimitY      = 1
}

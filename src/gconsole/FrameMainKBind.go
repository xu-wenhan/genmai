////////////////////////////////////////////////////////////////
//
//       Filename:  FrameMainKBind.go
//
//        Version:  1.0
//        Created:  2022年11月17日 10时06分18秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package gconsole

import (
    "github.com/jesseduffield/gocui"
)

// TODO: to make it can config
func
(fm *FrameMain)KeyBinding(g *gocui.Gui) error {
    rcs := []error{}

    // Program Command
    rcs = append(rcs, g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, fm.Quit))

    // FRAMEMAINVIEW_VIEWNAME_CONTROL
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyTab, gocui.ModNone, fm.NextView))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEMAINVIEW_VIEWNAME_CONTROL,
    //                  gocui.KeyCtrlH, gocui.ModNone, fm.PreView))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEMAINVIEW_VIEWNAME_CONTROL,
    //                  gocui.KeyCtrlL, gocui.ModNone, fm.NextView))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEMAINVIEW_VIEWNAME_CONTROL,
    //                  gocui.KeyArrowRight, gocui.ModAlt, fm.NextView))
    /* */
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyArrowDown, gocui.ModNone, fm.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyCtrlJ, gocui.ModNone, fm.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyArrowUp, gocui.ModNone, fm.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyCtrlK, gocui.ModNone, fm.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyCtrlN, gocui.ModNone, fm.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyCtrlP, gocui.ModNone, fm.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyEnter, gocui.ModNone, fm.NextView))
    /* */
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyCtrlD, gocui.ModNone, fm.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyCtrlU, gocui.ModNone, fm.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeySpace, gocui.ModNone, fm.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyBackspace, gocui.ModNone, fm.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEMAINVIEW_VIEWNAME_CONTROL,
                        gocui.KeyBackspace2, gocui.ModNone, fm.CursorPageUp))

    for _, rc := range rcs {
        if rc != nil {
            return rc
        }
    }
    return nil
}

func
(fm *FrameMain)Quit(g *gocui.Gui, v*gocui.View) error {
    return gocui.ErrQuit
}

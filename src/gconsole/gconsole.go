////////////////////////////////////////////////////////////////
//
//       Filename:  gconsole.go
//
//        Version:  1.0
//        Created:  2022年11月16日 21时59分38秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package gconsole

import (
    //"log"

    "github.com/jesseduffield/gocui"
    "main/genmai"
)

func
Draw() (rc error) {
    genmai.DoctorIns().Reset()
    /* */
    genmai.DoctorIns().LoadExplorersListConfig("data/KernelPocs/KernelPocs.yaml")
    genmai.DoctorIns().LoadExplorersListConfig("data/SystemPocs/SystemPocs.yaml")

    rps := genmai.DoctorIns().Genmai()

    G_FrameReport.Setup(rps)

    g := gocui.NewGui()
    g.Init()
    defer g.Close()

    g.SetLayout(layout)

    G_FrameReport.KeyBinding(g)

    g.SelBgColor = gocui.ColorGreen
    g.SelFgColor = gocui.ColorBlack
    g.Cursor = true
    rc = g.MainLoop()
    if ( (nil != rc) &&
         (gocui.ErrQuit != rc) ) {

    } // if ( (nil ...

    return rc
}

func
layout(g *gocui.Gui) error {
    rc := G_FrameReport.SetTargetLayout(g)
    /* */
    if (nil != rc) {
        return rc
    } // if (nil != ...

    rc = G_FrameReport.SetVulnLayout(g)
    if (nil != rc) {
        return rc
    } // if (nil != ...

    rc = G_FrameReport.SetVulnInfoLayout(g)
    if (nil != rc) {
        return rc
    } // if (nil != ...

    rc = G_FrameReport.SetReportLayout(g)
    if (nil != rc) {
        return rc
    } // if (nil != ...

    return nil
}

////////////////////////////////////////////////////////////////
//
//       Filename:  FrameMainView.go
//
//        Version:  1.0
//        Created:  2022年11月17日 10时07分32秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package gconsole

import (
    "github.com/jesseduffield/gocui"
)

const (
    FRAMEMAINVIEW_VIEWNAME_CONTROL          string = "control"
)

///////////////////////////////
// navigation

///////////////////////////////
// navigation
func
(fm *FrameMain)NextView(g *gocui.Gui, v *gocui.View) error {
    return nil
}
func
(fm *FrameMain)PreView(g *gocui.Gui, v *gocui.View) error {
    return nil
}
/* */
func
(fm *FrameMain)NextSummary(g *gocui.Gui, v *gocui.View) error {
    return nil
}
func
(fm *FrameMain)PreSummary(g *gocui.Gui, v *gocui.View) error {
    return nil
}
/* */
func
(fm *FrameMain)CursorDown(g *gocui.Gui, v *gocui.View) error {
    return nil
}
func
(fm *FrameMain)CursorUp(g *gocui.Gui, v *gocui.View) error {
    return nil
}
func
(fm *FrameMain)CursorPageDown(g *gocui.Gui, v *gocui.View) error {
    return nil
}
func
(fm *FrameMain)CursorPageUp(g *gocui.Gui, v *gocui.View) error {
    return nil
}

////////////////////////////////////////////////////////////////
//
//       Filename:  KeyBinding.go
//
//        Version:  1.0
//        Created:  2022年11月16日 22时30分16秒
//       Revision:  none
//       Compiler:  go
//
//         Author:  alpha
//   Organization:  alpha
//       Contacts:  chenxinquan@kylinos.com
//
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Description:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Log:
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
// Todo:
//
////////////////////////////////////////////////////////////////

package gconsole

import (
    "github.com/jesseduffield/gocui"
)

// TODO: to make it can config
func
(fr *FrameReport)KeyBinding(g *gocui.Gui) error {
    rcs := []error{}

    // Program Command
    rcs = append(rcs, g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, fr.Quit))

    // FRAMEREPORTVIEW_VIEWNAME_TARGET
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyTab, gocui.ModNone, fr.NextView))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEREPORTVIEW_VIEWNAME_TARGET,
    //                  gocui.KeyCtrlH, gocui.ModNone, fr.PreView))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEREPORTVIEW_VIEWNAME_TARGET,
    //                  gocui.KeyCtrlL, gocui.ModNone, fr.NextView))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEREPORTVIEW_VIEWNAME_TARGET,
    //                  gocui.KeyArrowRight, gocui.ModAlt, fr.NextView))
    /* */
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyArrowDown, gocui.ModNone, fr.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyCtrlJ, gocui.ModNone, fr.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyArrowUp, gocui.ModNone, fr.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyCtrlK, gocui.ModNone, fr.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyCtrlN, gocui.ModNone, fr.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyCtrlP, gocui.ModNone, fr.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyEnter, gocui.ModNone, fr.NextView))
    /* */
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyPgdn, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyPgup, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyCtrlD, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyCtrlU, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeySpace, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyBackspace, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_TARGET,
                        gocui.KeyBackspace2, gocui.ModNone, fr.CursorPageUp))

    // FRAMEREPORTVIEW_VIEWNAME_VULN
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyTab, gocui.ModNone, fr.NextView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyCtrlQ, gocui.ModNone, fr.PreView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyCtrlH, gocui.ModNone, fr.PreView))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEREPORTVIEW_VIEWNAME_VULN,
    //                  gocui.KeyCtrlL, gocui.ModNone, fr.NextView))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEREPORTVIEW_VIEWNAME_VULN,
    //                  gocui.KeyArrowLeft, gocui.ModAlt, fr.PreView))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEREPORTVIEW_VIEWNAME_VULN,
    //                  gocui.KeyArrowDown, gocui.ModAlt, fr.NextView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyArrowDown, gocui.ModNone, fr.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyArrowUp, gocui.ModNone, fr.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyCtrlJ, gocui.ModNone, fr.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyCtrlK, gocui.ModNone, fr.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyPgdn, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyPgup, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyCtrlD, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyCtrlU, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeySpace, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyBackspace, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyBackspace2, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyEnter, gocui.ModNone, fr.NextView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyCtrlN, gocui.ModNone, fr.NextVulnItem))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULN,
                        gocui.KeyCtrlP, gocui.ModNone, fr.PreVulnItem))

    // FRAMEREPORTVIEW_VIEWNAME_VULNINFO
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyTab, gocui.ModNone, fr.NextView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyCtrlQ, gocui.ModNone, fr.PreView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyCtrlH, gocui.ModNone, fr.NextView))
    //  rcs = append(rcs, g.SetKeybinding(FRAMEREPORTVIEW_VIEWNAME_VULNINFO, gocui.KeyCtrlL, gocui.ModNone, fr.NextView))
    //  rcs = append(rcs, g.SetKeybinding(FRAMEREPORTVIEW_VIEWNAME_VULNINFO, gocui.KeyArrowUp, gocui.ModAlt, fr.PreView))
    //  rcs = append(rcs, g.SetKeybinding(FRAMEREPORTVIEW_VIEWNAME_VULNINFO, gocui.KeyArrowLeft, gocui.ModAlt, fr.NextView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyArrowDown, gocui.ModNone, fr.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyArrowUp, gocui.ModNone, fr.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyCtrlJ, gocui.ModNone, fr.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyCtrlK, gocui.ModNone, fr.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyPgdn, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyPgup, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyCtrlD, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyCtrlU, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeySpace, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyBackspace, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyBackspace2, gocui.ModNone, fr.CursorPageUp))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
    //                  gocui.KeyCtrlM, gocui.ModNone, fr.CursorMoveMiddle))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyCtrlN, gocui.ModNone, fr.NextVulnItem))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyCtrlP, gocui.ModNone, fr.PreVulnItem))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_VULNINFO,
                        gocui.KeyEnter, gocui.ModNone, fr.NextView))

    // FRAMEREPORTVIEW_VIEWNAME_REPORT
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyTab, gocui.ModNone, fr.NextView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyCtrlQ, gocui.ModNone, fr.PreView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyCtrlH, gocui.ModNone, fr.NextView))
    //  rcs = append(rcs, g.SetKeybinding(FRAMEREPORTVIEW_VIEWNAME_REPORT, gocui.KeyCtrlL, gocui.ModNone, fr.NextView))
    //  rcs = append(rcs, g.SetKeybinding(FRAMEREPORTVIEW_VIEWNAME_REPORT, gocui.KeyArrowUp, gocui.ModAlt, fr.PreView))
    //  rcs = append(rcs, g.SetKeybinding(FRAMEREPORTVIEW_VIEWNAME_REPORT, gocui.KeyArrowLeft, gocui.ModAlt, fr.NextView))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyArrowDown, gocui.ModNone, fr.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyArrowUp, gocui.ModNone, fr.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyCtrlJ, gocui.ModNone, fr.CursorDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyCtrlK, gocui.ModNone, fr.CursorUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyPgdn, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyPgup, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyCtrlD, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyCtrlU, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeySpace, gocui.ModNone, fr.CursorPageDown))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyBackspace, gocui.ModNone, fr.CursorPageUp))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyBackspace2, gocui.ModNone, fr.CursorPageUp))
    //  rcs = append(rcs, g.SetKeybinding(
    //                  FRAMEREPORTVIEW_VIEWNAME_REPORT,
    //                  gocui.KeyCtrlM, gocui.ModNone, fr.CursorMoveMiddle))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyCtrlN, gocui.ModNone, fr.NextVulnItem))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyCtrlP, gocui.ModNone, fr.PreVulnItem))
    rcs = append(rcs, g.SetKeybinding(
                        FRAMEREPORTVIEW_VIEWNAME_REPORT,
                        gocui.KeyEnter, gocui.ModNone, fr.NextView))

    for _, rc := range rcs {
        if rc != nil {
            return rc
        }
    }
    return nil
}

func
(fr *FrameReport)Quit(g *gocui.Gui, v*gocui.View) error {
    return gocui.ErrQuit
}

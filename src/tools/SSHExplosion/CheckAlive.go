package SSHExplosion

import(
	"net"
	"fmt"
	"time"
)

func checkAlive(ip string) bool {
	alive := false
	_, err := net.DialTimeout("tcp", fmt.Sprintf("%v:%v", ip, "22"), time.Second*5)
	if err == nil {
	   alive = true
	}
	return alive
 }
package SSHExplosion
import(
	// "log"
	// "io"
	"os"
	"bufio"
    "strings"
)

func readFile(filename string) ([]string, error) {
    file, err := os.Open(filename)
    if err != nil {
       return nil, err
    }
    defer file.Close()
    scanner := bufio.NewScanner(file)
    scanner.Split(bufio.ScanLines)
    var result []string
    for scanner.Scan() {
       passwd := strings.TrimSpace(scanner.Text())
       if passwd != "" {
          result = append(result, passwd)
       }
    }
    return result, err
}
package SSHExplosion

import(
	"fmt"
)

func SshExp(ipList []string ,nums int){
	readNameFile,err:=readFile("../data/dic/name.txt")
	readPWDFile,err1:=readFile("../data/dic/dic.txt")
	fmt.Println(readNameFile,err,err1)
	vul:=make(map[string]interface{})
	aliveIP:=checkAlivePool(ipList[:],nums)

	vul["readNameFile"]=readNameFile
	vul["readPWDFile"]=readPWDFile
	vul["ip"]=aliveIP
	vul["nums"]=nums
	SSHCoprogram(vul)
}
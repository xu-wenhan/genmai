package SSHExplosion

import (

    "fmt"

    "golang.org/x/crypto/ssh"
    // "golang.org/x/crypto/ssh/agent"
    // "log"

    "time"

)
func SshConnect(ip, username, password string) (bool, error) {
    success := false
    config := &ssh.ClientConfig{
       User: username,
       Auth: []ssh.AuthMethod{
          ssh.Password(password),
       },
       Timeout:         5 * time.Second,
       HostKeyCallback: ssh.InsecureIgnoreHostKey(),
    }
    client, err := ssh.Dial("tcp", fmt.Sprintf("%v:%v", ip, 22), config)
    if err == nil {
       defer client.Close()
       session, err := client.NewSession()
       errRet := session.Run(" ")
       if err == nil && errRet == nil {
          defer session.Close()
          success = true
       }
    }
    return success, err
}
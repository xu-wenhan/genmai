import itertools
import argparse

string = ''
f = open('../data/dic/dic.txt', 'a')

 
def listtostring(list):
    return str(list).replace("('", '').replace("',),", '\n').replace('[', '').replace(']', '').replace("'),",
                                                                                                       '\n').replace(
        "',)", '').replace("', '", '').replace("')", '').replace(' ', '')
 
if __name__ == '__main__':
    # 解析参数
    parser = argparse.ArgumentParser(description='Jenkins pipline parameters')
    parser.add_argument('--CompanyName', type=str, default='0',)
    parser.add_argument('--Name', type=str, default='0',)
    parser.add_argument('--Nums', type=int, default='0',)
    args = parser.parse_args()
 
    CompanyName = args.CompanyName
    Name = args.Name
    Nums = args.Nums

    #大小写转换
    if str(CompanyName.islower())=="True":
        CompanyNameUP=CompanyName.upper()
        CompanyNameCP=CompanyName.capitalize()
    else:
        CompanyNameLW=CompanyName.lower()
        CompanyNameUP=CompanyNameLW.upper()
        CompanyNameCP=CompanyNameLW.capitalize()

    if str(Name.islower())=="True":
        NameUP=Name.upper()
        NameCP=Name.capitalize()
    else:
        NameLW=Name.lower()
        NameUP=NameLW.upper()
        NameCP=NameLW.capitalize()

    for i in range(1, 4):
        List = list(
            itertools.permutations(
                ['2020', 'ADMIN', 'admin', '..', '1234', 'pass', 'PASSWORD', 'password', '123', '123456', '!@#','!','@','#','~','!@','@#','qwer','QWER','QWE','qwe'
                 'administrator', 'PASS', '12345', '1234567890', '2021', '','.','2022','pw','abc','abcd','aaa','ABCD','abcd',CompanyName,Nums,Name, CompanyNameUP,CompanyNameCP,NameCP,NameUP],
                i))
        string = listtostring(List)
        f.write(string + "\n")
    f.close()